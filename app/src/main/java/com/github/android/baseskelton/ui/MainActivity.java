package com.github.android.baseskelton.ui;

import android.os.Bundle;

import com.github.android.baseskelton.R;
import com.github.android.baseskelton.base.BaseActivity;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NavController navController = Navigation.findNavController(
                this, R.id.nav_host_fragment);
    }

    @Override
    public boolean onSupportNavigateUp() {
        return Navigation.findNavController(this, R.id.nav_host_fragment)
                .navigateUp();
    }
}
