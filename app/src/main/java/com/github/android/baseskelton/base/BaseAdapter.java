package com.github.android.baseskelton.base;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public abstract class BaseAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    protected void loadImageFromURL(@NonNull Context context, @NonNull ImageView view,
                                    @NonNull String url, @Nullable Object signature) {

        if (TextUtils.isEmpty(url)) {
            return;
        }

        Picasso.with(context)
                .load(url)
                .into(view);
    }
}
